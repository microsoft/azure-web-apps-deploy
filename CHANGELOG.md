# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 1.0.4

- patch: Remove support for debug mode in az webapps deploy command in order to prevent Azure CLI to expose the deployment password in the logs.

## 1.0.3

- patch: Update support link and license to Microsoft

## 1.0.2

- patch: FIX typo in README.md examples

## 1.0.1

- patch: Update license to MIT

## 1.0.0

- major: Initial release.

